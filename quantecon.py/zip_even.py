#!/usr/bin/env python3

x_vals = 1, 2, 3, 4, 5, 6
y_vals = 2, 2, 4, 4, 5, 8
values = zip(x_vals, y_vals)

for i in values:
    a, b = i 
    if (a % 2 == 0 and b % 2 == 0):
        print(i)

