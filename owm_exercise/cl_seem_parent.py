#!/usr/bin/python

class Parents:

	sexFemale = "Female"
	sexMale   = "Male"

	def __init__(self, name, skin, hair, size): 
		self.name = name
		self.skin = skin
		self.hair = hair
		self.size = size

#	def __str__(self):
#		return "Parent Tile: %s, Name: %s, Color Skin: %s, Hair: %s, Size: %2.2f, Sex: %s" % (self.title, self.name, self.skin\
#			, self.hair, self.size)

	def mother(self):
		title = "Mommy"
		print "Parent Tile: %s, Name: %s, Color Skin: %s, Hair: %s, Size: %2.2f, Sex: %s" % (title, self.name, self.skin\
			, self.hair, self.size, Parents.sexFemale)


	def father(self): 
		title = "Daddy"
		print "Parent Tile: %s, Name: %s, Color Skin: %s, Hair: %s, Size: %2.2f, Sex: %s" % (title, self.name, self.skin\
			, self.hair, self.size, Parents.sexMale)


ParentA = Parents("Mariam", "White", "Large", 6.1)
ParentB = Parents("Roman", "Black", "Short", 7.4)

ParentA.mother()
ParentB.father()
