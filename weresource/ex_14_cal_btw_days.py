#!/usr/bin/python3

from datetime import datetime

print("Python Script - Show days between A & B days.\nEnter date format: [1992, 4, 10].\n")

datefmt	= "%Y, %m, %d"
adate	= input("Enter First date: ")
bdate	= input("Enter Second date: ")
print(" ")
#ainfo	= datetime.strptime(adate, datefmt)
def lessday(adate, bdate):
	x	= datetime.strptime(adate, datefmt)
	y	= datetime.strptime(bdate, datefmt)
	print("Days between Dates: [%d days]" % (abs(x - y).days))
	return

lessday(adate, bdate)