#!/usr/bin/python3

from math import pi

print("Python3 Script - Calc volumen of a Sphere.\n")

rd	= float(input("Enter radius: "))

def decorator(func):
	def wrapp(decorator):
		print("\n++++++++++++++++++++++++++++")
		func(decorator)
		print("++++++++++++++++++++++++++++\n")
	return wrapp

@decorator
def Sphera(rd):
	result	= (4/3 * pi * rd ** 3)
	print("Volumen of Sphere: [%.2f]" % result)

Sphera(rd)