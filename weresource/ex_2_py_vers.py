#!/usr/bin/python

import sys

class Pyversion:
	"""Python version"""

	def displayVersion(self):
		version	= sys.version
		print "Python Version: \n%s\n" % version

	def displayVersionInfo(self):
		version	= sys.version_info
		print "Python Version Info: \n%s\n" % version

pv = Pyversion()

pv.displayVersion()
pv.displayVersionInfo()