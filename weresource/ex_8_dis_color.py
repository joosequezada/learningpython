#!/usr/bin/python3

print("Python Script - Show first and last color.")
print(" ") 

color_list = ["Red","Green","White" ,"Black"]

first = color_list[0]
last  = color_list[-1]

print("Color list:  %s" % color_list)
print("First color: %s \nLast color:  %s" % (first, last))
