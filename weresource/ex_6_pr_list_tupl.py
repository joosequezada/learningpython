#!/usr/bin/python3

print ("Python Script - Show list & tuple formats.")
print("==========================================")
print ("Type data separate by comma.")
print(" ")

data = input("Enter data: ")
datalist = data.split(",")
datatupl = tuple(datalist)
print(" ")
print("Data A - List format: ", datalist)
print("Data A Type: ", (type(datatupl)))
print(" ")
print("Data B - Tuple format: ", datatupl)
print("Data B Type: ", (type(datalist)))


