#!/usr/bin/python

STR =  'Hello World'
NMB =   3

print STR[0]
print STR[2:5]
print STR[2:]
print STR * 2
print NMB * 2
print STR + ' Cruel'
print NMB + 1
