#!/usr/bin/python

dic             = {}
dic[2]          = "This is Two"
dic['three']    = 'This is Three'
listdic         = {'name': "Luisa", 'code': 4324, 'dept': 'it'}

print dic['three']
print dic[2]
print listdic
print listdic.keys()
print listdic.values()
print listdic['name']

