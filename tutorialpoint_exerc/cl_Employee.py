#!/usr/bin/python

class Employee:
	"""Employees and their salaries"""
	empCount = 0 

	def __init__(self, name, salary):
		self.name	= name
		self.salary	= salary
		Employee.empCount += 1

	#def displayCount(self):
	#	print "Total Employees: %d" % Employee.empCount

	def displayEmployee(self):
		print "Name: ", self.name, ", Salary: ", self.salary

emp1	= Employee("Jhon", 40000)
emp2	= Employee("Mariam", 56000)
emp3	= Employee("Camila", 60000)

emp1.displayEmployee()
emp2.displayEmployee()
emp3.displayEmployee()

print "Total Employee: %d" % Employee.empCount
