#!/usr/bin/python

def changeme(mylist):
	"Change list into a function"
	mylist.append([1,3,4,5, "hola"])
	print "Values inside function: ", mylist
	return

mylist = [10,50, "mundo"]
changeme(mylist)
print "Values outside function: ", mylist



def changem(mlist):
	"Change list into a function"
	mlist = ([1,3,4,5, "hola"])
	print "Values inside function: ", mlist
	return

mlist = [10,50, "mundo"]
changem(mlist)
print "Values outside function: ", mlist