#!/usr/bin/python

TUPLE        =  ( 'abc', 43, 7, "hola", 0.5, 'world' )
TUPLEEXT     =  (123, "cruel")

print   TUPLE
print   TUPLE[0]
print   TUPLE[3:6]
print   TUPLE[2:]
print   TUPLE[2] * 3
print   TUPLE[0] * 2
print   TUPLE + TUPLEEXT #Solo concatena, no TUPLEas y string. 
