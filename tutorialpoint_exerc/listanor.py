#!/usr/bin/python

LIST        =  ['abc', 43, 7, "hola", 0.5, 'world']
LISTEXT     =  [ "cruel"]

print   LIST
print   LIST[0]
print   LIST[3:6]
print   LIST[2:]
print   LIST + LISTEXT #Solo concatena listas, no listas y string. 
print   LIST[2] * 3
print   LIST[0] * 2
