#!/usr/bin/python


def person( name, sex, age = 18 ):
	print "Name : ", name
	print "Age: ", age
	print "Sex: ", sex
	print "---------------"
	return person;


person( name="Jhon", sex="Male", age = 24 )
person( name="Maria", sex="Female" )
person( "Juana", "Male", 24 )

