#!/usr/bin/python

rw_input  	= raw_input("Enter a filename: ")
mode 		= raw_input("Enter mode: ")
doc 		= open(rw_input, mode)

print "Name of file: ", doc.name
print "Closed or not: ", doc.closed
print "Opening mode: ", doc.mode
print "Softspace flag: ", doc.softspace

doc.close()
