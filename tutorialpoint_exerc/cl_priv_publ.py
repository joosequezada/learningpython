#!/usr/bin/python 

class Orga:
	publicOrg		= str("Im Public!")
	__privateOrg	= str("Im Private!")

	def publOrg(self):
		self.publicOrg
		print self.publicOrg

	def privOrg(self):
		self.__privateOrg
		print self.__privateOrg

org = Orga()
print "From Inside class: ", org.publOrg()
print "From Inside class: ", org.privOrg()

print "From Outside class: ", org.publicOrg
#print "From Outside class: ", org.__privateOrg
print "From Outside class: ", org._Orga__privateOrg
