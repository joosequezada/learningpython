#!/usr/bin/python 

import os, sys, stat



fd = os.open("foo.txt", os.O_RDONLY)
os.fchmod(fd, stat.S_IXGRP)
os.fchmod(fd, stat.S_IRWXU)

print "Changed mode successfully!"

os.close(fd)

