#!/usr/bin/python

try:
	fn = open("foo.txt", "w")
	fn.write("Writting something to file.")
except IOError:
	print "Error: Can't find file or read it."
else:
	print "Written content in the file successfully!"