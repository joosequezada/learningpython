#!/usr/bin/python

import os, sys

fd = os.open("foo.txt", os.O_RDWR|os.O_CREAT)

print "%s" % os.pathconf_names

no = os.fpathconf(fd, "PC_LINK_MAX")
print "Max number of link to the file: %d" % no

no = os.fpathconf(fd, "PC_NAME_MAX")
print "Max legth to the file: %d" % no

os.close(fd)

print "Closed file successfully!"