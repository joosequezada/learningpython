#!/usr/bin/python

try:
	fn = open("foo.txt", "w")
	try:
		fn.write("Writting something to file.")
	finally: 
		print "Going to close this file."
		fn.close()		
except IOError:
	print "Error: Can't find file or read it."
#else:
#	print "Written content in the file successfully!"